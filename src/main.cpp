#include "gui/application.hpp"

int main(int argc, char *argv[])
{
  gui::Application app(argc, argv);

  return app.run();
}
