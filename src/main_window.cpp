#include "gui/main_window.hpp"

#include "QMessageBox"
#include <iostream>

constexpr int SECONDS_PER_MINUTE = 60;

MainWindow::MainWindow(QWidget* parent)
  : QMainWindow(parent), m_secTimer(new QTimer(this)),
    m_mode(CurrentMode::NOT_WORKING)
{
  setupUi(this);
  setup();
}

void MainWindow::setup()
{
  connect(controlButton,
          &QPushButton::clicked,
          this,
          &MainWindow::onControlButtonClick);
  connect(m_secTimer, &QTimer::timeout, this, &MainWindow::onTimerTick);
}

void MainWindow::stopCountDown()
{
  m_secTimer->stop();
  m_mode = CurrentMode::NOT_WORKING;
  intervalEdit->setEnabled(true);
  controlButton->setText("Start");
  m_secondsLeft = 0;
  updateTimeLeftLabel();
}

void MainWindow::startCountDown()
{
  m_secTimer->start(1000);
  m_mode = CurrentMode::COUNT_DOWN;
  intervalEdit->setEnabled(false);
  m_intervalInSeconds = intervalEdit->value() * SECONDS_PER_MINUTE;
  m_secondsLeft = intervalEdit->value() * SECONDS_PER_MINUTE;
  controlButton->setText("Stop");
  updateTimeLeftLabel();
}

void MainWindow::onControlButtonClick()
{
  if(m_mode == CurrentMode::COUNT_DOWN)
    stopCountDown();
  else
    startCountDown();
}

void MainWindow::updateTimeLeftLabel()
{
  QString timeLeftStr = QString("%0:%1")
                          .arg(m_secondsLeft / SECONDS_PER_MINUTE)
                          .arg(m_secondsLeft % SECONDS_PER_MINUTE);
  timeLeftLabel->setText(timeLeftStr);
}

void MainWindow::onTimerTick()
{
  --m_secondsLeft;

  updateTimeLeftLabel();

  if(m_secondsLeft <= 0)
  {
    QMessageBox::information(this, "Time for pause", "Have a break");
    m_secondsLeft = m_intervalInSeconds;
  }
}
